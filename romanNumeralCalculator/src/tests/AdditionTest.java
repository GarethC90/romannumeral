/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.impl.RomanNumeralAddition;

/**
 * @author eflophi
 *
 */
public class AdditionTest {
    RomanNumeralAddition add = new RomanNumeralAddition();

    @Test
    public void testCompact() {
        assertEquals("IV", add.compact("IIII"));
        assertEquals("IX", add.compact("VIIII"));
        assertEquals("XL", add.compact("XXXX"));
        assertEquals("XC", add.compact("LXXXX"));
        assertEquals("CD", add.compact("CCCC"));
        assertEquals("CM", add.compact("DCCCC"));
        assertEquals("MCCXIV", add.compact("MCCXIIII"));
    }

    @Test
    public void testUncompact() {
        assertEquals("IIII", add.uncompact("IV"));
        assertEquals("VIIII", add.uncompact("IX"));
        assertEquals("XXXX", add.uncompact("XL"));
        assertEquals("LXXXX", add.uncompact("XC"));
        assertEquals("CCCC", add.uncompact("CD"));
        assertEquals("DCCCC", add.uncompact("CM"));
        assertEquals("CCCLXVIIII", add.uncompact("CCCLXIX"));
        assertEquals("DCCCXXXXV", add.uncompact("DCCCXLV"));
    }

    @Test
    public void testSort() {
        assertEquals("VII", add.sort("IIV"));
        assertEquals("XXVIII", add.sort("VIIIXX"));
        assertEquals("CXXVI", add.sort("IVXCX"));
        assertEquals("MDCLXVI", add.sort("IVXLCDM"));
        assertEquals("DCCCCCCLXXXXXVVIIII", add.sort("CCCLXVIIIIDCCCXXXXV"));
    }

    @Test
    public void testcombineGroups() {
        assertEquals("X", add.combineGroups("VV"));
        assertEquals("C", add.combineGroups("LL"));
        assertEquals("M", add.combineGroups("DD"));
        assertEquals("V", add.combineGroups("IIIII"));
        assertEquals("L", add.combineGroups("XXXXX"));
        assertEquals("D", add.combineGroups("CCCCC"));
        assertEquals("MCCXIIII", add.combineGroups("DCCCCCCLXXXXXXIIII"));
    }

}
