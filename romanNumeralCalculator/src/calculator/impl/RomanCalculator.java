/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

import calculator.iface.ICalculator;

/**
 * @author eflophi Class to calculate two roman numerals that are input by the user in chosen ways. It will return roman numerals as strings and does
 *         not use decimals/ints in its calculations.
 */
public class RomanCalculator implements ICalculator {

    private final RomanNumeralAddition add = new RomanNumeralAddition();

    @Override
    public String add(final String num1, final String num2) {
        return add.add(num1, num2);
    }
}
