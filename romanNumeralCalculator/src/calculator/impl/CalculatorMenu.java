/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

/**
 * @author eflophi class to show a menu to the user for the string calculator
 *
 */
public class CalculatorMenu {
    private final IOFunctionality io = new IOFunctionality();
    private final RomanNumeral romanNumeral = new RomanNumeral();
    private final RomanCalculator calc = new RomanCalculator();

    /** Displays a menu to the user for the String calculator */
    public void display() {
        io.out("Enter First Number: ");
        final String num1 = io.readLine();
        io.out("Enter Second Number: ");
        final String num2 = io.readLine();

        if (romanNumeral.validateInput(num1) && romanNumeral.validateInput(num2))
            io.out(calc.add(num1, num2));
        else {
            io.out("numbers aren't valid roman numerals");
            display();
        }
    }
}
