/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package main;

import calculator.impl.CalculatorMenu;

/**
 * @author eflophi Class to start the claculator program
 */
public class Main {
    private Main() {
    }

    /**
     * launches the menu
     *
     * @param args
     */
    public static void main(final String[] args) {
        final CalculatorMenu calc = new CalculatorMenu();
        calc.display();
    }
}
