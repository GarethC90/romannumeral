/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.impl.RomanNumeral;

/**
 * @author eflophi
 *
 */
public class ValidationTest {

    RomanNumeral num = new RomanNumeral();

    @Test
    public void testValidateInput() {
        assertEquals(true, num.validateInput("X"));
        assertEquals(true, num.validateInput("XXII"));
        assertEquals(false, num.validateInput("IVS"));
        assertEquals(false, num.validateInput("K"));
        assertEquals(true, num.validateInput("MDCCCXLVI"));
    }

    @Test
    public void testValidateInputWithTooManyLetters() {
        assertEquals(false, num.validateInput("VIIII"));
        assertEquals(false, num.validateInput("CMCMDDXII"));
        assertEquals(false, num.validateInput("VVV"));
        assertEquals(false, num.validateInput("LLL"));
    }

    @Test
    public void testValidateInputWithWrongOrder() {
        assertEquals(false, num.validateInput("IIIVII"));
        assertEquals(false, num.validateInput("XXLXX"));
        assertEquals(false, num.validateInput("MCCCDC"));
    }
}
