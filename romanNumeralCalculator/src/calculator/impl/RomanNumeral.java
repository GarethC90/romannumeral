/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import calculator.iface.IRomanNumeral;

/**
 * @author eflophi Class used for validation of Roman Numeral Strings. Contains rules and functionality for checking if a roman numeral is valid or
 *         not
 *
 */
public class RomanNumeral implements IRomanNumeral {

    protected static final char[] numOrder = { 'I', 'V', 'X', 'L', 'C', 'D', 'M' };

    /**
     * checks if the string is a real roman numeral
     *
     * @param userInput
     *            - string that was entered
     *
     * @return true if correct
     */
    @Override
    public boolean validateInput(final String userInput) {
        final String validNumber = "^[I,V,X,L,C,D,M]+$";
        final Pattern pattern = Pattern.compile(validNumber);
        final Matcher matcher = pattern.matcher(userInput);
        final boolean isValid = matcher.matches();

        for (int i = 0; i < numOrder.length; i++) {
            int occurenceCount = 0;
            for (final char x : userInput.toCharArray()) {
                if (x == numOrder[i])
                    occurenceCount++;
                if ((i % 2 == 0 && occurenceCount == 4) || (i % 2 == 1 && occurenceCount == 2))
                    throw new NumberFormatException("value contains illegal amount of characters");
            }
        }

        for (int i = 0; i < numOrder.length - 1; i++) {
            final String test = new String(new char[2]).replace('\0', numOrder[i]);
            if (userInput.indexOf(test) > -1 && (userInput.indexOf(test) < userInput.indexOf(numOrder[i + 1])))
                throw new NumberFormatException("value contains illegal characters");
        }
        return isValid;
    }
}
