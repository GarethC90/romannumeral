/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import calculator.impl.RomanCalculator;

/**
 * @author eflophi
 *
 */
public class CalculatorTest {

    RomanCalculator calc = new RomanCalculator();

    @Test
    public void testAddNumbersWithBasicSums() {
        // basic sums
        assertEquals("XII", calc.add("VII", "V"));
        assertEquals("IX", calc.add("IV", "V"));
        assertEquals("III", calc.add("I", "II"));
        assertEquals("CMXXII", calc.add("CM", "XXII"));
        assertEquals("LXXIV", calc.add("XIV", "LX"));
    }

    @Test
    public void testAddNumbersWithDoubles() {
        assertEquals("X", calc.add("V", "V"));
        assertEquals("M", calc.add("D", "D"));
        assertEquals("C", calc.add("L", "L"));
        assertEquals("MCX", calc.add("DLV", "DLV"));
    }

    @Test
    public void testAddNumbersEnforcingRules() {
        assertEquals("VIII", calc.add("III", "V")); // reverse order both should work
        assertEquals("VIII", calc.add("V", "III"));
        assertEquals("CXV", calc.add("CIX", "VI")); // complex sums
        assertEquals("LXX", calc.add("LIV", "XVI"));
        assertEquals("XXII", calc.add("XIX", "III"));
    }

    @Test
    public void testAddWhereResultShouldHaveASymbolChange() {
        assertEquals("IV", calc.add("II", "II"));
        assertEquals("VIII", calc.add("IV", "IV"));
        assertEquals("V", calc.add("II", "III"));
        assertEquals("XL", calc.add("XX", "XX"));
        assertEquals("CM", calc.add("DCC", "CC"));
        assertEquals("MDCCCXLVI", calc.add("DCCXII", "MCXXXIV"));
        assertEquals("MCCXIV", calc.add("CCCLXIX", "DCCCXLV"));
    }
}