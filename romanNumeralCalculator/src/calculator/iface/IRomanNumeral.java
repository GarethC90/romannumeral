/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.iface;

/**
 * @author eflophi
 *
 */
public interface IRomanNumeral {
    boolean validateInput(final String userInput);
}
