/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.impl;

/**
 * @author eflophi Class to return the sum of two roman numeral strings
 */
public class RomanNumeralAddition {

    private static final char[] numOrder = RomanNumeral.numOrder;
    private static final CharSequence[] subtractives = { "IV", "IX", "XL", "XC", "CD", "CM" };

    /**
     * returns the addition of the specified Roman Numerals as a Roman Numeral. main algorithm: remove subtractives, concatenate numbers, sort,
     * combine groups of repeating symbols, add subtractives
     *
     * @param num1
     *            first roman numeral to be added
     * @param num2
     *            second roman numeral to be added
     * @return roman numeral String
     */
    public String add(final String num1, final String num2) {
        String result;
        String numeral1 = num1;
        String numeral2 = num2;

        numeral1 = uncompact(numeral1);
        numeral2 = uncompact(numeral2);
        result = numeral1 + numeral2;
        result = sort(result);
        result = combineGroups(result);
        result = compact(result);
        return result;
    }

    /**
     * Substitutes for any subtractives using array of subtractives and array of numbers. eg 'IV' becomes 'IIII'
     *
     * @param num
     *            roman numeral to uncompact
     * @return the string with substituted roman numerals
     */
    public String uncompact(final String num) {
        String result = num;
        for (int i = 0; i < subtractives.length; i++) {
            if (result.contains(subtractives[i]) && isEven(i)) {
                result = result.replace(subtractives[i], buildStringSequence(4, numOrder[i]));
                break;
            }
            else if (result.contains(subtractives[i]) && !isEven(i)) {
                result = result.replace(subtractives[i], numOrder[i] + buildStringSequence(4, numOrder[i - 1]));
            }
        }
        return result;
    }

    /**
     * substitutes subtractives where possible eg 'IIII' becomes 'IV'
     *
     * @return the string with substituted roman numerals
     * @param num
     *            - string to be changed
     */
    public String compact(final String num) {
        String result = num;
        for (int i = 0; i < numOrder.length - 1; i++) {
            int countSymbols = 0;
            for (final char x : result.toCharArray()) {
                if (x == numOrder[i] || x == numOrder[i + 1])
                    countSymbols++;
            }

            final String longform = buildStringSequence(4, numOrder[i]);
            if (countSymbols > 4)
                result = result.replace(numOrder[i + 1] + longform, subtractives[i + 1]);
            else if (countSymbols > 3)
                result = result.replace(longform, subtractives[i]);
        }
        return result;
    }

    /**
     * sorts the Roman numeral left to right in order of biggest to smallest
     *
     * @return the sorted Roman numeral
     * @param num
     *            - an unsorted Roman numeral
     */
    public String sort(final String num) {
        final StringBuilder result = new StringBuilder(15);
        for (int i = numOrder.length - 1; i > -1; i -= 1) {
            for (final char x : num.toCharArray()) {
                if (x == numOrder[i])
                    result.append(numOrder[i]);
            }
        }
        return result.toString();
    }

    /**
     * combines values that scale up (eg 'VV' becomes 'X', 'LL' becomes 'C', etc..) V,L and D can only be used once. I,X,C,M can only be used three
     * times
     *
     * @return combined string
     * @param num
     *            - string to be changed
     */
    public String combineGroups(final String num) {
        String result = num;
        for (int i = 0; i < numOrder.length; i++) {
            String group;
            int countVLD = 0;
            int countIXCM = 0;
            for (final char x : result.toCharArray()) {
                if (x == numOrder[i] && !isEven(i))
                    countVLD++;
                else if (x == numOrder[i] && isEven(i))
                    countIXCM++;

                if (countVLD > 1) {
                    group = buildStringSequence(2, numOrder[i]);
                    result = result.replace(group, Character.toString(numOrder[i + 1]));
                }
                else if (countIXCM > 4) {
                    group = buildStringSequence(5, numOrder[i]);
                    result = result.replace(group, Character.toString(numOrder[i + 1]));
                }
            }
        }
        return result;
    }

    /** checks if a number is even and returns a boolean */
    private boolean isEven(final int number) {
        if (number % 2 == 0)
            return true;
        return false;
    }

    /** returns roman numeral sequence of specified character a specificed amount of times */
    private String buildStringSequence(final int length, final char romanNumeral) {
        return new String(new char[length]).replace('\0', romanNumeral);
    }
}