/**
 * -----------------------------------------------------------------------
 *     Copyright (C) 2016 LM Ericsson Limited.  All rights reserved.
 * -----------------------------------------------------------------------
 */
package calculator.iface;

/**
 * @author eflophi Interface for calculator class
 */
public interface ICalculator {

    /**
     * returns the addition of the specified Roman Numerals as a Roman Numeral. *
     *
     * @param num1
     *            first roman numeral to be added
     * @param num2
     *            second roman numeral to be added
     * @return roman numeral String
     */
    public String add(String num1, String num2);

}